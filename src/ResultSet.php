<?php
namespace MetzOhanian\Yapo;
use \PDO;

class ResultSet implements \JsonSerializable {
	private $STH;
	var $fieldset;
	var $__ERROR;
	var $__SQL_STATEMENT;

	function __construct($StatementHandle, $sql) {
		$this->STH = $StatementHandle;
		$this->__ERROR[] = array($sql, $this->STH->errorCode(), $this->STH->errorInfo());
		$this->__SQL_STATEMENT = $sql;
	}
	
	function Size() {
		return $this->STH->rowCount();
	}
	
	function Next() {
		$data = @$this->STH->fetch(PDO::FETCH_OBJ);
		if ($data) {
			foreach ($data as $field => $value) {
				$this->$field = $value;
				$this->fieldset[$field] = $value;
			}
			return true;
		}
		return false;
	}
	
  function jsonSerialize() {
    return $this->fieldset;
  }
  
  public function toArray() {
    return json_decode(json_encode($this), true);
  }
	
  public function toObject() {
    return json_decode(json_encode($this));
  }
	
	function CurrentFieldSet() {
	    return $this->fieldset;
	}
	
	function HasError() {
		return is_array($this->__ERROR) && count($this->__ERROR) > 0 && $this->__ERROR[0][1] != "00000";
	}
	
	function GetErrors() {
		return array_map(function ($e) { return $e[2][2]; }, $this->__ERROR);
	}
}