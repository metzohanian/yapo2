<?php
namespace MetzOhanian\Yapo;

class Db {

	protected $DBH;
	
	protected $Data;
	
	protected $Debug = false;
	
	protected $__lastsql;
	
	var $__ERRORS = array();
	
	var $__SetupParameters = array();
	
	var $__RowCount;
	
	private $__constructor_args;
	
	const AUTO_INT_ID = 'auto_int_id';
	const UUID_ID = 'uuid_id';
	
	function __construct() {
		$this->__constructor_args = func_get_args();
	}
	
	public function GetDb($db) {
		$r = new ReflectionClass("\\MetzOhanian\\Yapo\\Driver\\Database\\$db");
		return $r->newInstanceArgs($this->__constructor_args);
	}
	
	public function TableExists($table) {
	}

	function SetDebug($debug) {
		$this->Debug = $debug;
	}
	
	function TableDescription($table) {
	}	
	
	function GetLastInsertId($TableSequence = null) {
		return $this->DBH->lastInsertId($TableSequence);
	}
	
	function GetCore($table) {
		return new Core($this, $table);
	}
	
	function Query($sql, $DataSet = null) {
		$this->__lastsql = $sql;
		if (is_array($DataSet))
			$this->SetData($DataSet);
		return $this->DataSet($sql);
	}
	
	function BeginTransaction() {
		$this->DBH->beginTransaction();
	}
	
	function Commit() {
		$this->DBH->commit();
	}
	
	function RollBack() {
		$this->DBH->rollBack();
	}
	
	function SetAliasedField($field, $alias, $value) {
		$this->Data[":$alias"] = $value;
	}
	
	function SetAliasedData($Data) {
		$this->Data = array();
		foreach ($Data as $d => $fieldinfo) {
			$this->SetAliasedField($fieldinfo->field, $fieldinfo->alias, $fieldinfo->value);
		}
	}
	
	function RowCount() {
		return $this->__RowCount;
	}
	
	function Execute($sql) {
		$this->__lastsql = $sql;
		if ($this->Debug) {
			echo $sql;
			print_r($this->Data);
		}
		$Query = $this->DBH->prepare($sql);
		if (count($this->Data) > 0)
			$Query->execute($this->Data);
		else
			$Query->execute();
		$failed = $this->handle_errors(1, $Query);
	}
	
	function DataSet($sql) {
		$this->__lastsql = $sql;
		$Query = $this->DBH->prepare($sql);
		if (is_countable($this->Data) && count($this->Data) > 0)
			$Query->execute($this->Data);
		else
			$Query->execute();
		$failed = $this->handle_errors(1, $Query);
		return new ResultSet($Query, $sql);
	}
	
	function handle_errors($cnt, $Query) {
		return false;
	}

	function Clear() {
		$this->Data = array();
	}
	
	function __set($field, $value) {
		if (property_exists($this, $field))
			throw new Exception("You may not access protected members of Yapo DB: " . __FILE__ . ":" . __LINE__ . ": $field <- $value" . "\n\n" . print_r(debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT), true));
		$this->Data[":$field"] = $value;
	}
	
	function SetData($Data) {
		$this->Data = $Data;
	}
	
	function ValidateField($field_def, $value) {
		return false;
	}
	
	function GetStructureDriver($structure, $factory, $values) {

	}
	
	function FixPrimarySequence($table, $sequence, $primaryKey) {
		
	}

}

// Polyfills

if (!function_exists('is_countable')) {
    function is_countable($var) {
        return (is_array($var) || $var instanceof Countable);
    }
}



?>