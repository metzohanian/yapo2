<?php
namespace MetzOhanian\Yapo\Structures;

class AuditEx extends Audit {

	var $__save_callback;
	var $__delete_callback;
	
	function __construct(& $database = null, $table = null, $save_callback = null, $delete_callback = null, $table_audit = null, $audit_fields = null) {
		if (is_null($database))
			return;
		
		$this->__save_callback = $save_callback;
		$this->__delete_callback = $delete_callback;
		
		parent::__construct($database, $table, 
													is_null($table_audit) ? 
														$table . '_history' : $table_audit, 
													is_null($audit_fields) ? 
														array( 'modified_by_id', 'modified_at', 'deleted', 'delete_stmt' ) : 
														array_unique(array_merge($audit_fields, array('modified_at', 'deleted', 'delete_stmt'))));

	}
	
	public function find($advance_recordset = true, $params = array()) {
		if (isset($this->fields()['status'])) {
			$field_vals = $this->core()->GetFieldValues();
			if (!isset($field_vals['status'])) {
				$this->status = 'active';
			}
		}
		return parent::find($advance_recordset, $params);
	}
	
	
	public function save($all = false) {
		if (is_callable($this->__save_callback)) {
			$save = $this->__save_callback;
			$save($this->HistoryYapo());
		}
		
		$this->HistoryYapo()->modified_at = date("Y-m-d H:i:s");
		$this->HistoryYapo()->deleted = 0;
		$this->HistoryYapo()->delete_stmt = '';
		$pk = $this->primarykey();
		$fields = $this->core()->GetFieldValues();
		unset($fields[$pk]);
		$this->HistoryYapo()->fields = json_encode(array_keys($fields));
		return parent::save($all);
	}
	
	public function delete() {
		$this->HistoryYapo()->clear();
		$primary_state = $this->saveState();
		foreach ($this->fields() as $field_name => $desc) {
			$this->HistoryYapo()->$field_name = $this->anyvalue($field_name);
		}

		parent::delete();

		if (is_callable($this->__delete_callack))
			$this->__delete_callack($this->HistoryYapo());
		
		$this->HistoryYapo()->modified_at = date("Y-m-d H:i:s");
		$this->HistoryYapo()->deleted = 1;
		$this->HistoryYapo()->delete_stmt = json_encode(array($this->lastSql(), $this->lastData()));

		return $this->HistoryYapo()->save();
	}
}

?>