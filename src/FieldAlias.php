<?php
namespace MetzOhanian\Yapo;

class FieldAlias {
	var $field;
	var $alias;
	var $value;
	var $field_name;
	
	function __construct($field, $alias, $value) {
		$this->field = $field;
		$this->alias = $alias;
		$this->value = $value;
		
		$field_name = explode('.', $field);
		if (count($field_name) > 0) {
			$this->field_name = str_replace('"', "", $field_name[count($field_name)-1]);
		}
	}
}

?>