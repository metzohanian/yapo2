<?php
namespace MetzOhanian\Yapo\Components;

class Guid {
	
	static function random() {
		return Guid::randomBytes(16);
	}
	
	static function hotSpot() {
		$time = explode(" ", microtime());
		$utime = base_convert(bcadd($time[1], 12219292800) . substr($time[0], 2, 7), 10, 16);
		return pack("H*", substr($utime, 0, 12) . 1 . substr($utime, 12)) . Guid::randomBytes(8);
	}
	
	static function hotSequence($sequence = null, $location = null) {
		$location = isset($location) ? $location+1 : 1;
		if (!isset($sequence)) $sequence = mt_rand(0, 255);
		$h = unpack("C*", Guid::hotSpot());
		$h[1] = $sequence;
		array_unshift($h, "C*");
		return call_user_func_array("pack", $h);
	}
	
	static function version4() {
		$rand = unpack("C*", Guid::random());
		$rand[7] = (0x0F & $rand[7]) | (0x40);
		$rand[9] = (0x3F & $rand[9]) | (0x80);
		array_unshift($rand, "C*");
		return call_user_func_array("pack", $rand);
	}

	static function toCanonical($guid) {
		$hex = bin2hex($guid);
		return substr($hex, 0, 8) . '-' . substr($hex, 8, 4) . '-' . substr($hex, 12, 4) . '-' . substr($hex, 16, 4) . '-' . substr($hex, 20);
	}
	
	static function randomBytes($n) {
		$bytes = random_bytes($n);
		return str_pad($bytes, $n - strlen($bytes), 0x0);
	}

  static function isGuidFormat($str) {
    return preg_match("/([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}/", $str); 
  }

}

?>