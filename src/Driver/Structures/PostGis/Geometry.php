<?php
namespace MetzOhanian\Yapo\Structures\PostGis;

class Geometry {
	function __construct() {
		$this->elements = func_get_args();
		if (count($this->elements) == 2)
			list($this->x, $this->y) = $this->elements;
		if (count($this->elements) == 3)
			list($this->x, $this->y, $this->z) = $this->elements;
	}
}

?>