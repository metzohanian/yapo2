<?php
namespace MetzOhanian\Yapo\Driver\Database;
use \PDO;

class Mysql extends \MetzOhanian\Yapo\Db {

	function __construct($host, $dbname, $user, $password, $err_mode = PDO::ERRMODE_SILENT) {
		$this->DBH = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'", PDO::MYSQL_ATTR_LOCAL_INFILE => true));
		$this->DBH->setAttribute(PDO::ATTR_ERRMODE, $err_mode);
	}
	
	public function TableExists($table) {
		$this->Clear();
		$table = $this->DataSet("show tables like '$table'");
		return $table->Size() == 1;
	}
	
	function TableDescription($table) {
		$this->Clear();
		$Keys = $this->DataSet("SHOW KEYS IN $table");
		if ($Keys->HasError())
			throw new \Exception(implode("\n",$Keys->GetErrors()));
		
		$Fields = $this->DataSet("describe $table");
		if ($Fields->HasError())
			throw new \Exception(implode("\n",$Fields->GetErrors()));
		
		$this->Clear();
		
		$keys = array();
		
		while ($Keys->Next()) {
			if (!isset($keys[$Keys->Key_name]) || !is_array($keys[$Keys->Key_name]))
				$keys[$Keys->Key_name] = array('Unique'=>!$Keys->Non_unique,'Columns'=>array());
			$keys[$Keys->Key_name]['Columns'][] = $Keys->Column_name;
		}
		
		
		$fields = array();
		$primary_key = false;
		$primary_key_type = false;
		while ($Fields->Next()) {
			preg_match("/(.+)\((.+)\)/", $Fields->Type, $matches);
			$fields[$Fields->Field] = array(
					'MajorType' => count($matches) < 3 ? $Fields->Type : $matches[1],
					'MinorType' => count($matches) < 3 ? $Fields->Type : $matches[2],
					'Type' => $Fields->Type,
					'Null' => $Fields->Null=="NO"?false:true,
					'Key' => $Fields->Key,
					'Extra' => $Fields->Extra
				);
			if (strtoupper($Fields->Key) == 'PRI') {
				$primary_key = $Fields->Field;
				switch ($fields[$Fields->Field]['MajorType']) {
					case 'int':
					case 'bigint':
            if ($fields[$Fields->Field]['Extra'] == 'auto_increment')
  						$primary_key_type = \MetzOhanian\Yapo\Db::AUTO_INT_ID; break;
					case 'binary':
						$primary_key_type = \MetzOhanian\Yapo\Db::UUID_ID; break;
				}
			}
		}
		
		if ($primary_key === false) {
			throw new \Exception("Yapo($table) requires a primary key.");
    }
		
		if ($primary_key_type === false) {
			throw new \Exception("Yapo($table) auto increment-type tables require the auto_increment indicator, which should be on the primary key.");
    }
			
		return array("Keys" => $keys, "Fields" => $fields, "PrimaryKey" => $primary_key, "PKeyType" => $primary_key_type);
	}	
	
	function GetCore($table) {
		return new \MetzOhanian\Yapo\Driver\Core\Mysql($this, $table);
	}
		
	function SetAliasedField($field, $alias, $value) {
		$this->Data[":$alias"] = $value;
	}
	
	function SetAliasedData($Data) {
		$this->Data = array();
		foreach ($Data as $d => $fieldinfo) {
			$this->SetAliasedField($fieldinfo->field, $fieldinfo->alias, $fieldinfo->value);
		}
	}
	
	function handle_errors($cnt, $Query) {
		$this->__RowCount = $Query->rowCount();
		if ($cnt==0) return true;
		switch ($Query->errorCode()) {
			case '00000': return true;
			case 'HY200':
					$this->DBH = new PDO($this->__SetupParameters[0], $this->__SetupParameters[1], $this->__SetupParameters[2]);
					$this->DBH->exec('set names utf8');
					$this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
				return false;
			default: return true;
		}
	}
	
	function ValidateField($field_def, $value) {
		if (stristr($field_def['MajorType'], 'int') || 
			stristr($field_def['MajorType'], 'float') || 
			stristr($field_def['MajorType'], 'double') || 
			stristr($field_def['MajorType'], 'real') ||
			stristr($field_def['MajorType'], 'decimal') ||
			stristr($field_def['MajorType'], 'numeric')) {
			return $value;
		} else if (strtoupper($field_def['MajorType']) == 'TIME') {
			return "'" . date("H:i:s", strtotime($value)) . "'";
		} else if (stristr($field_def['MajorType'], 'time')) {
			return "'" . date("Y-m-d H:i:s", strtotime($value)) . "'";
		} else if (stristr($field_def['MajorType'], 'date')) {
			return "'" . date("Y-m-d", strtotime($value)) . "'";
		} else if (strtoupper($field_def['MajorType']) == 'YEAR') {
			return "'" . date("Y", strtotime($value)) . "'";
		} else if (stristr($field_def['MajorType'], 'text')) {
			// incomplete
		}
	}
	
	function GetStructureDriver($structure, $factory, $values) {
		$driver_class = "\\MetzOhanian\\Yapo\\Driver\\Structures\\Mysql\\$structure";
		$params = array_merge((array)$driver_class, $values);
		return call_user_func_array($factory, $params);
	}

}


?>
