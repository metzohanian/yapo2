<?php
namespace MetzOhanian\Yapo\Driver\Actions\PostgreSql;

class Find extends \MetzOhanian\Yapo\Actions\Find {
		
	function PaginationSql() {
        list($pagination, $page) = $this->Core->GetLimit();
		$lsql = '';
        if (!is_null($pagination)) {
            $p = $page * $pagination;
            $lsql = " limit $pagination offset $p";
        }
		return $lsql;
	}
		
}

?>