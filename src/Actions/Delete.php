<?php
namespace MetzOhanian\Yapo2;

namespace MetzOhanian\Yapo\Actions;

class Delete extends Action {
	function __construct(& $Core, & $Where) {
		parent::__construct($Core);
		$this->Where = & $Where;
	}
	
	function GenerateSql($params) {
		parent::GenerateSql($params);
		if (is_array($params))
			extract($params);
		
		$sql = "delete from {$this->Core->__table} ";
		
		list($wsql, $fields) = $this->Where->GenerateSql($params);
		
		return array($sql . $wsql, $fields);
	}
}

?>