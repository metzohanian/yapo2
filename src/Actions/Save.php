<?php
namespace MetzOhanian\Yapo2;

namespace MetzOhanian\Yapo\Actions;

class Save extends Action {
	var $Where;
	var $Mode;

	function __construct(& $Core, & $Where) {
		parent::__construct($Core);
		$this->Where = & $Where;
	}
	
	function GenerateSql($params) {
		parent::GenerateSql($params);
		if (is_array($params))
			extract($params);
		
		if (!$this->Core->HasActiveRecord() && !$all && !$this->Core->MismatchedSetEquals()) {
			// insert new record
			$this->Mode = "insert";
			return $this->insert();
		} else if (!$this->Core->HasActiveRecord() || $all) {
			// update set
			$this->Mode = "update_set";
			return $this->update_set();
		} else if ($this->Core->HasActiveRecord() || $this->Core->PrimaryKeyIsSet()) {
			// update current record
			$this->Mode = "update";
			return $this->update();
		} else {
			// not sure how we got here ...
		}
	}

	function GenerateInsertIntoSql(& $Find, $target_table, $insert_fields, $source_fields) {
		list($sql, $Data) = $Find->GenerateSql(array());
		
		switch ($this->Core->TableInsertType()) {
			case \MetzOhanian\Yapo\Db::AUTO_INT_ID:
				$sql = "insert into $target_table (" . implode(",",$insert_fields) . ") " . $sql;
				break;
			case \MetzOhanian\Yapo\Db::UUID_ID:
				throw new \Exception("Insert Into not supported by UUID tables yet.");
				break;
		}
		
		return array($sql, $Data);
		
	}
	
	protected function typeMassage($field, $value) {
		return $value;
	}
	
	protected function insert() {
		$sql = "insert into {$this->Core->__table} ";
		$insert_fields = array();
		if ($this->Core->TableInsertType() == \MetzOhanian\Yapo\Db::UUID_ID) {
			if (!$this->Core->PrimaryKeyIsSet()) {
				$pkey = $this->Core->GetPrimaryKeyField();
				$this->Core->$pkey = \MetzOhanian\Yapo\Components\Guid::hotSequence();
				$this->Core->Comparator($pkey, \MetzOhanian\Yapo\Yapo::EQUALS, $this->Core->$pkey);
				$this->Core->Comparator($pkey, \MetzOhanian\Yapo\Yapo::SET, $this->Core->$pkey);
			}
		}
		foreach ($this->Core->__definition as $field => $definition) {
			if (isset($definition['Null']) && strtoupper($definition['Null']) == 'NO') {
				$insert_fields[$this->Core->GetFieldName($field, false)] = "";
			}
		}
		foreach ($this->Core->__field_actions as $field => $comparator) {
			if (isset($comparator[\MetzOhanian\Yapo\Yapo::SET])) {
				$insert_fields[$this->Core->GetFieldName($field, false)] = $comparator[\MetzOhanian\Yapo\Yapo::SET];
			}
		}
		$sql .= "(" . implode(", ", array_keys($insert_fields)) . ")";
		$fields = array();
		foreach ($insert_fields as $field => $value) {
			$value = $this->typeMassage($field, $value);
			$fields["insert_" . str_replace('.','_',$field)] = new \MetzOhanian\Yapo\FieldAlias($field, "insert_" . str_replace('.','_',$field), $value);
		}
		$sql .= " values (" . implode(", ", array_map(function($n) { return ":$n"; }, array_keys($fields))) . ")";
		return array($sql, $fields);
	}
	
	protected function update_set() {
		list($sql, $update_fields) = $this->update_base();
		list($wsql, $where_fields) = $this->Where->GenerateSql(array());
		$sql .= $wsql;
		$update_fields = array_merge($update_fields, $where_fields);
		
		return array($sql, $update_fields);
	}
	
	protected function update() {
		list($sql, $update_fields) = $this->update_base();
		$primary_key = $this->Core->GetPrimaryKeyField();
		$this->Core->Comparator($primary_key, \MetzOhanian\Yapo\Yapo::EQUALS, $this->Core->$primary_key);
		list($wsql, $where_fields) = $this->Where->GenerateSql(array('primary' => true));
		$sql .= " " . $wsql;
		$update_fields = array_merge($update_fields, $where_fields);
		return array($sql, $update_fields);
	}
	
	protected function update_base() {
		$sql = "update {$this->Core->__table} set ";
		$fields = array();
		$update_fields = array();
		foreach ($this->Core->__field_actions as $field => $comparator) {
			if (isset($comparator[\MetzOhanian\Yapo\Yapo::SET])) {
				$fields[] = $this->Core->GetFieldName($field) . " = :update_" . $this->Core->GetQualifiedName($field, '_'); 
				$update_fields["update_" . $this->Core->GetQualifiedName($field, '_')] = 
					new \MetzOhanian\Yapo\FieldAlias($field, "update_" . $this->Core->GetQualifiedName($field, '_'), $comparator[\MetzOhanian\Yapo\Yapo::SET]);
			}
		}
		$sql .= implode(', ', $fields) . ' ';
		return array($sql, $update_fields);
	}

}

?>